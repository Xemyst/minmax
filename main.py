import os
import gameTree as gt
Max = 'X'
Min = 'O'

def main():
	global winner, gtree
	gtree = gt.GameTree()
	winner = False
	user = input("chose sign (X or O): ").upper()
	game = True

	while game:
		#user chose X

		if user == 'X':
			while not winner:

				os.system('clear')
				gtree.printBoard()
				userMove()

				if not winner:
					computerMove()

		#user chose O
		else:
			while not winner:
				os.system('clear')
				computerMove()
				gtree.printBoard()
				if not winner:
					userMove()
					
		os.system('clear')
		gtree.printBoard()
		print("Winner : ", player)
		repeat = input("Do you want to play again(y/n): ").upper()

		if repeat == 'Y':
			game = True
			winner = False
			gtree.restart()
		else:
			game = False

def userMove():
	global gtree, winner, player

	print('turn:', gtree.state.turn )
	move = int(input('insert move(0 - 8):'))
	gtree.move(move)
	winner, player = gtree.evaluate()

def computerMove():
	global gtree, winner, player

	x = gtree.minMax()
	gtree.move(x[1])
	winner, player = gtree.evaluate()	

if __name__ == '__main__':
	main()