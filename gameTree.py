class GameTree(object):
	def __init__(self):
		print("generating tree game...")
		self.initNode = Node()
		self.state = self.initNode
	def move(self,move):
		a = self.state.move(move)
		if a:
			self.state = a[0]
		else:
			while not a:
				self.printBoard()
				print('turn:', self.state.turn )
				move = int(input('insert move(0 - 8):'))
				a = self.state.move(move)
				if a:
					self.state = a[0]
	def evaluate(self):
		return self.state.evaluate()

	def minMax(self):
		return self.state.minMax()		

	def printBoard(self):
		print("{}|{}|{}".format(self.state.board[0],self.state.board[1],self.state.board[2]))
		print("-"*5)
		print("{}|{}|{}".format(self.state.board[3],self.state.board[4],self.state.board[5]))
		print("-"*5)
		print("{}|{}|{}".format(self.state.board[6],self.state.board[7],self.state.board[8]))

	def restart(self):
		self.state = self.initNode
		
class Node(object):
	gameState = {0:' ',1:' ',2:' ',3:' ',4:' ',5:' ',6:' ',7:' ',8:' '}
	def __init__(self,gameBoard = gameState,move=10,turn = -1):
		self.turn = (turn + 1)
		self.board = gameBoard
		#move defined to 10 for defaul because just the initial call will be without move, and this will indicate that is the empty board case
		if move != 10:
			if (self.turn & 1) == 1:
				self.board[move] = 'X'
			else:
				self.board[move] = 'O'
		self.moves = []
		gameBoard = self.board
		finalState = self.evaluate()[0]
		if not finalState:
			for x in range (0,9):
				if self.board[x] == ' ':
					#these will save the nodes with move options, and the move number to go to him
					self.moves += [[Node(self.board.copy(),x,self.turn),x]]

	def minMax(self):

		finalState, winner = self.evaluate()
		options = []
		#final state
		if finalState:
			if winner == 'O':
				#fist de value, second just for fill the gap, third de distance
				return -1,'',0
			elif winner == 'X':
				return 1, '',0
			else:
				return 0, '',0

		else:
			for move in self.moves:
				value = move[0].minMax()
				#first the value of the move, second the move, third the distance to the final state
				options +=  [[ value[0],move[1],value[2] ]]
			candidate = options[0]

			#even number, X turn maximize
			if (self.turn & 1) == 0:
				for option in options:
					#searching the lowest value move and the shortest
					if option[0] > candidate[0] or (option[0] >= candidate[0] and option[2] < candidate[2]):
						candidate = option
				#increase the distance
				candidate[2] += 1
				return candidate

			#odd number, O turn minimize
			if (self.turn & 1) == 1:
				for option in options:
					if option[0] < candidate[0] or (option[0] <= candidate[0] and option[2] < candidate[2]):
						candidate = option
				candidate[2] += 1
				return candidate




	def canMove(self):

		for x in range (0,9):
				if self.board[x] == ' ':
					return True
		return False


	def move(self,move):
		
		for candidate in self.moves:
			if move in candidate:
				return candidate

	def evaluate(self):
		#evaluate colums
		if self.board[4] != ' ':
			#vertical 2
			if self.board[1] == self.board[4] == self.board[7]:
				return True, self.board[4]
			#horitzontal 2
			if self.board[3] == self.board[4] == self.board[5]:
				return True, self.board[4]
			#diagonals
			elif self.board[0] == self.board[4] == self.board[8]:
				return True , self.board[4]
			elif self.board[2] == self.board[4] == self.board[6]:
				return True, self.board[4]

		if self.board[0] != ' ':
			#horitzontal 1
			if self.board[0] == self.board[1] == self.board[2]:
				return True, self.board[0]
			#vertical 1
			if self.board[0] == self.board[3] == self.board[6]:
				return True, self.board[0]
			
		if self.board[8] != ' ':
			#horitzontal 3
			if self.board[6] == self.board[7] == self.board[8]:
				return True, self.board[8]
			#vertical 3
			if self.board[2] == self.board[5] == self.board[8]:
				return True, self.board[8]
		if not self.canMove():
			return True, 'Empate'
		
		return False, self.board[4]